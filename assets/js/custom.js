(function ($) {
  'use strict';

  $(document).ready(function () {
    const json = {
      week: {
        goal: 50,
        currentScore: 38,
        leftTime: 12,
        currentStreak: 4,
        bestStreak: 12,
        leaders: [
          {
            position: 1,
            fullName: "Walter Wynne",
            score: 105,
            positionChange: "up"
          },
          {
            position: 2,
            fullName: "Annabel Ferdinand",
            score: 52,
            positionChange: "no"
          },
          {
            position: 3,
            fullName: "Marty McFly",
            score: 50,
            positionChange: "up"
          },
          {
            position: 7,
            fullName: "You!",
            score: 38,
            positionChange: "up"
          }
        ]
      },
      month: {
        goal: 50,
        currentScore: 12,
        leftTime: 186,
        currentStreak: 7,
        bestStreak: 44,
        leaders: [
          {
            position: 1,
            fullName: "Annabel Ferdinand",
            score: 120,
            positionChange: "no"
          },
          {
            position: 2,
            fullName: "Walter Wynne",
            score: 105,
            positionChange: "up"
          },
          {
            position: 3,
            fullName: "John Doe",
            score: 104,
            positionChange: "down"
          },
          {
            position: 13,
            fullName: "You!",
            score: 12,
            positionChange: "down"
          }
        ]
      }
    };

    function checkPercent(i, max) {
      if (i > max) {
        checkPercent(i - max, max);
      }

      return i / max;
    }

    function getData(period = 'week') {
      const data = json[period];

      const widget = $('#widget');
      const timePeriod = widget.find('[data-time-period]');
      const currentScore = widget.find('[data-current-score]');
      const goal = widget.find('[data-goal]');
      const leftTime = widget.find('[data-left-time]');
      const currentStreak = widget.find('[data-current-streak]');
      const bestStreak = widget.find('[data-best-streak]');

      timePeriod.text(period);
      currentScore.text(data.currentScore);
      goal.text(data.goal);
      leftTime.text(data.leftTime);
      currentStreak.text(data.currentStreak);
      bestStreak.text(data.bestStreak);

      const progress = widget.find('svg>path.progress-bar');
      const max = parseInt(progress.attr('stroke-dasharray'));
      const percent = checkPercent(data.currentScore, data.goal);
      progress.attr({
        'stroke-dashoffset': parseInt(max + max * percent)
      });

      const list = $('#leaderboard li');
      list.addClass('hidden');

      setTimeout(function() {
        $.each(data.leaders, function(i, obj) {
          const rank = list.eq(i); // positionChange
          const position = rank.find('[data-position]');
          const fullName = rank.find('[data-full-name]');
          const score = rank.find('[data-score]');
  
          position.text(obj.position);
          fullName.text(obj.fullName);
          score.text(obj.score);
          rank.attr({
            'position-change': obj.positionChange
          });
        });

        list.removeClass('hidden');
      }, 1000);
    }

    getData($('#time-period option:selected').val());
    $('#time-period').on('change', function () {
      getData(this.value);
    });
  });
})(jQuery);