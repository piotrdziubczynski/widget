/**
 * npm init
 * npm install
 * npm install babel-core babel-preset-es2015 browser-sync gulp gulp-autoprefixer gulp-babel gulp-connect-php gulp-imagemin gulp-sass gulp-sourcemaps gulp-uglifyjs imagemin-pngquant --save-dev
 */

'use strict';

let gulp					= require('gulp'),
		sass					= require('gulp-sass'),
		autoprefixer	= require('gulp-autoprefixer'),
		sourcemaps		= require('gulp-sourcemaps'),
		babel					= require('gulp-babel'),
		uglify				= require('gulp-uglifyjs'),
		imagemin			= require('gulp-imagemin'),
		pngquant			= require('imagemin-pngquant'),
		browserSync		= require('browser-sync'),
		connect				= require('gulp-connect-php');

gulp.task('imagemin', function () {
	return gulp.src('./assets/images/**/*')
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest('./images'));
});

gulp.task('sass', function () {
  gulp.src('./assets/sass/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(autoprefixer('> 0.5%', 'last 7 versions', 'Firefox ESR', 'not dead', 'not ie < 10'))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./css'));
});

gulp.task('uglify', function() {
	gulp.src('./assets/js/**/*.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(uglify('custom.min.js'))
		.pipe(gulp.dest('./js'));
});

gulp.task('watch', function() {
	connect.server({}, function() {
		browserSync.init({
			proxy: 'http://localhost:8080/widget/',
			open: true,
			keepalive: true,
		});
	});

	gulp.watch('./assets/sass/**/*.scss', ['sass']);
	gulp.watch('./assets/js/**/*.js', ['uglify']);
	gulp.watch('./assets/images/**/*', ['imagemin']);
	gulp.watch(['./css/*.css', './js/*.js', './images/*', './**/*.html']).on('change', function() {
		browserSync.reload();
	});
});
